import datetime
import json
import logging
import threading
import time 
import traceback

import wrapt

from kivy.utils import get_color_from_hex

from kivy_tastypie.config import orm_settings

@wrapt.decorator
def background_thread(wrapped, instance, args, kwargs):
    t = threading.Thread(target = wrapped, args = args, kwargs = kwargs)
    t.start()

@wrapt.decorator
def timeit(wrapped, instance, args, kwargs):
    ts = time.time()
    result = wrapped(*args, **kwargs)
    te = time.time()
    print("function time {} : {} sec".format(wrapped.__name__, te - ts))
    return result

class Timer():

    def __enter__(self):
        """Start a new timer as a context manager"""
        self.start()
        return self

    def __exit__(self, *exc_info):
        """Stop the context manager timer"""
        self.stop()

    def __init__(self, name = None):
        if name:
            self.name = f' ({name})'
        else:
            self.name = ''
        self._start_time = None

    def start(self):
        """Start a new timer"""
        self._start_time = time.perf_counter()


    def stop(self):
        """Stop the timer, and report the elapsed time"""
        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        print(f"Elapsed time{self.name}: {elapsed_time:0.4f} seconds")

def coerce_value_to_python(value):
    return str_to_date(value)

def convert_to_json(data):
    for key, value in data.items():
        if isinstance(value, datetime.date):
            data[key] = date_to_str(value)
    return data

def date_to_str(date_obj):
    if isinstance(date_obj, datetime.datetime):
        for time_format in orm_settings.REMOTE_DATETIME_FORMAT:
            try:
                return date_obj.strftime(time_format)
            except:
                pass
    elif isinstance(date_obj, datetime.date):
        for time_format in orm_settings.REMOTE_DATE_FORMAT:
            try:
                return date_obj.strftime(time_format)
            except:
                pass

def hex_to_rgba(hex_color, opacity = 1):
    c = list(get_color_from_hex(hex_color))
    c[-1] = opacity
    return c


def str_to_date(date_str):

    for date_format in orm_settings.REMOTE_DATE_FORMAT:
        try:
            return datetime.datetime.strptime(value, date_format)
        except:
            pass

    for date_format in orm_settings.REMOTE_DATETIME_FORMAT:
        try:
            return datetime.datetime.strptime(value, date_format)
        except :
            pass

    return date_str

def write_to_log(msg, level = 'info', include_traceback = False):
    if 'Kivy Tastypie:' not in msg:
        msg = f'Kivy Tastypie: {msg}'
    log_call = getattr(logging, level)
    log_call(msg)
    if include_traceback or level == 'error':
        log_call(traceback.format_exc())
