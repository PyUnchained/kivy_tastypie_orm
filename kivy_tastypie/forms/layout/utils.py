from copy import deepcopy
from functools import partial
import importlib, inspect
import logging

from .errors import LayoutUndefinedError
from kivy_tastypie.config import orm_settings

DEFAULT_LAYOUTS = {}

def get_default_layout(model_name, form_type):
    auto_detect_layouts()
    key = f"{model_name}+{form_type}"
    default_key = f"{model_name}+view"

    if key in DEFAULT_LAYOUTS:
        LayoutCls = DEFAULT_LAYOUTS[key]
    else:
        try:
            LayoutCls = DEFAULT_LAYOUTS[default_key]
        except KeyError:
            raise LayoutUndefinedError('Failed to find Layout class for '
                f'"{model_name}" model. Have you defined a layout class '
                'for it that inherits from BaseLayout?')
    return LayoutCls()

def auto_detect_layouts():
    from .base import BaseLayout

    if not DEFAULT_LAYOUTS:
        def is_layout(value):
            if inspect.isclass(value):
                if value != BaseLayout and issubclass(value, BaseLayout):
                    return True
            return False

        for name, cls in inspect.getmembers(importlib.import_module(orm_settings.LAYOUT_DEFINITIONS),
            is_layout):
            try:
                key = f"{cls.model_name}+{cls.form_type}"
                DEFAULT_LAYOUTS[key] = cls
            except AttributeError as e:
                pass

