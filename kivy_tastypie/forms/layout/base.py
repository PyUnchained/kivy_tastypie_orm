from copy import deepcopy
import importlib, inspect

class BaseLayout():

    add_layout = None
    can_delete = True
    edit_layout = None
    field_layout = []
    field_meta = {}
    fields = []
    form_type = None
    hidden_fields = ['id', 'resource_uri', 'created', 'last_updated', 'user', 'lft','rght',
        'level', 'tree_id', 'row', 'column']

    @classmethod
    def as_dict(cls):
        return {'fields':cls.fields, 'field_layout':cls.field_layout,
            'field_meta':cls.field_meta}

    @classmethod
    def copy_layout(self, layout, *args, **kwargs):
        return deepcopy(layout)

    @classmethod
    def flattened_form_layout(cls, form_type):
        resp = None
        if form_type == 'add':
            if cls.add_layout:
                resp = cls.add_layout.as_dict()
        elif form_type == 'edit':
            if cls.edit_layout:
                resp = cls.edit_layout.as_dict()
        if not resp:
            resp = cls.as_dict()

        resp['can_delete'] = cls.can_delete
        return resp

    @property
    def dict(self):
        return self.__class__.as_dict()

    def on_validate(self, form, *args, **kwargs):
        return True
