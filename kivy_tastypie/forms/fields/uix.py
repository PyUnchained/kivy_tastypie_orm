import copy, functools
import itertools

from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import (BooleanProperty, ObjectProperty, ListProperty,
    NumericProperty, StringProperty)
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.behaviors import ButtonBehavior


from kivy_tastypie.utils import background_thread
from contrib.fonts import icon

Builder.load_string("""
#: import icon contrib.fonts.icon

<SelectManyWidget>:
    orientation: 'horizontal'

<SelectableEntry>:
    orientation: 'horizontal'
    padding: [10,5]

    canvas.before:
        Color:
            rgba: self.bg_color
        Rectangle:
            pos: self.pos
            size: self.size

        Color:
            rgba: [1,1,1,1]
        Line:
            width: 2
            rectangle: self.x, self.y, self.width, self.height

    Label:
        text: root.entry_text
        color: root.color

    SelectableLabel:
        id: check_box
        size_hint_x: None
        width: '50dp'
        markup: True
        text: icon('fa-check-square')
        on_press: root.handle_select()





<M2MRecycleView>:
    RecycleBoxLayout:
        id: content_holder
        default_size: None, dp(56)
        default_size_hint: 1, None
        size_hint_y: None
        height: self.minimum_height
        orientation: 'vertical'



""")

class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
    RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''
    pass

class SelectableLabel(ButtonBehavior, Label):
    pass

class SelectableEntry(RecycleDataViewBehavior, BoxLayout):
    entry_text = StringProperty('')
    selected = BooleanProperty(False)
    bg_color = ListProperty([1,1,1,0])
    color = ListProperty([0,0,0,1])
    value_register = {}

    def handle_select(self):
        """ Behaviour for when one of the options is selected. """

        toggle_value = True # Flag determining whether or not the item's selection
                            # state will be toggled

        # Do nothing if readonly
        if self.field_widget.meta.readonly:
            toggle_value = False

        # If there is a select validator specified in the meta for the field use that
        elif self.field_widget.meta.input_validator:
            toggle_value = self.field_widget.meta.input_validator(self.obj,
                self.field_widget, self.value_register[self.entry_id])
            if not toggle_value:
                self.field_widget.invalid_animation.start(self.field_widget)

        # If the value is determined to need toggling, do so
        if toggle_value:
            self.value_register[self.entry_id] = not self.value_register[self.entry_id]
            self.refresh_display(user_select = True)

    def refresh_display(self, user_select = False,  *args):
        """ Called whenever data changes or is initally uploaded. """            
        self.toggle_selected_state()
        self.draw_bg()

    def refresh_view_attrs(self, rv, index, data):
        super().refresh_view_attrs(rv, index, data)
        if self.entry_id not in self.value_register:
            self.value_register[self.entry_id] = self.selected
        self.ids['check_box'].font_size = self.field_widget.input_font_size
        self.refresh_display()

    def draw_bg(self, *args):
        """ Determine what background color to set depending current item selection. """
        if not self.is_selected:
            self.bg_color = [0,0,0,0.1]
        else:
            self.bg_color = self.selection_bg_color

    def toggle_selected_state(self):

        # Handle new selection event
        if self.is_selected:
            self.ids['check_box'].text = icon('fa-check-square')
            if self.obj.resource_uri not in self.field_widget.value:
                self.field_widget.value.append(self.obj.resource_uri)

        # Handle deselection event
        else:
            self.ids['check_box'].text = icon('fa-square-o')
            if self.obj.resource_uri in self.field_widget.value:
                self.field_widget.value.remove(self.obj.resource_uri)
        self.field_widget.selected_count = len(self.field_widget.value)

    @property
    def is_selected(self):
        return self.value_register[self.entry_id] == True


class M2MRecycleView(RecycleView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.viewclass = SelectableEntry

class SelectManyWidget(BoxLayout):
    field_widget = ObjectProperty({})
    id_generator = itertools.count()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.build_widget()

    @background_thread
    def build_widget(self, *args):
        # Resize parent field widget
        self.field_widget.height = self.field_widget.height*4
        self.field_widget.orientation = 'vertical'
        self.field_widget.ids['label'].size_hint = (1, 0.2)
        self.field_widget.ids['label'].halign = 'center'
        self.size_hint = (1, 0.8)
        # Add selection boxes
        self.choice_list = M2MRecycleView(size_hint = (1,1))
        self.set_options_data()
        self.add_widget(self.choice_list)

    def set_base_opacity(self, item_index):
        if item_index % 2 == True:
            return 0
        else:
            return 0.1

    @property
    def qs(self, *args):
        return self.field_widget.get_queryset()

        
    def get_data_list(self, *args,**kwargs):
        data_list = []
        data_qs = self.qs

        for x in data_qs:
            # Determine if the item should be displayed as already selected
            if x.resource_uri in self.field_widget.value:
                selected = True
            else:
                selected = False
            # Doc to create label entry
            entry_dict = {'entry_text': str(x),
                'obj':x,
                'selected':selected, 'entry_id':next(self.id_generator),
                'selection_bg_color': [60/255,60/255,60/255,0.5],
                'field_widget':self.field_widget,
                'color':self.field_widget.form.label_color,
                'primary_color':self.field_widget.form.primary_color,
                'secondary_color':self.field_widget.form.secondary_color,
                'tertiary_color':self.field_widget.form.tertiary_color} 
            data_list.append(entry_dict)
        return data_list

    @property
    def data_widgets(self):
        for c in self.choice_list.children[0].children:
            yield c

    def set_options_data(self, *args):
        data_list = [{'entry_text': f"{x}"} for x in self.qs]
        self.choice_list.data = self.get_data_list()