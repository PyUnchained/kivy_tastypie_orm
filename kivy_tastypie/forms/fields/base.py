import copy
import datetime
import itertools
import logging 
import random
import re
import traceback

from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics import *
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import (ObjectProperty, StringProperty, NumericProperty, BooleanProperty,
    ListProperty)
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from kivy_tastypie.config import orm_settings, import_class
from kivy_tastypie.uix.button import PrimaryButton, DropDownButton
from kivy_tastypie.utils import hex_to_rgba, Timer, background_thread
from .uix import SelectManyWidget

Builder.load_string("""

<GenericField>:
    padding: [5,0]
    size_hint: (1,None)

    canvas.before:
        Color:
            rgba: self.color
            
        Rectangle:
            pos: self.pos
            size: self.size

    FieldLabel:
        id: label
        text: root.displayed_name
        font_size:root.label_font_size
        size_hint:root.label_size_hint
        halign: 'center'
        height: root.label_height
        markup: True

<FieldGroup>:
    padding: [0,0]
    orientation: 'horizontal'
    size_hint: (1,None)
    height: '70dp'

<FieldLabel>:
    text_size: self.size
    halign: 'left'
    valign: 'middle'
    size_hint: (0.5, 1)

<ClickableLabel>:
    text_size: self.size
    halign: 'center'
    valign: 'middle'
    inner_padding: 5
    size_hint: (None, None)
    height: '30dp'
    width: "30dp"
    font_size: '20dp'

<BooleanField>:
    
    CheckBox:
        id: check_box

""")

class FieldGroup(BoxLayout):
    
    def validate(self):
        is_valid = True
        for element in self.children:
            if not element.validate():
                is_valid = False
        return is_valid

class FieldLabel(Label):
    pass

class FieldMeta():
    """ Object representing the meta data of a field as provided by TastyPie """

    default_attributes = {'blank':True, 'default':None, 'help_text':'',
        'nullable':False, 'primary_key':False, 'readonly':False,
        'related_schema':None, 'related_type':None, 'type':None,
        'unique':False, 'verbose_name':'', 'choices':[],
        'long_string':False, 'hidden':False, 'min': None, 'max':None,
        'queryset_filter':{}, 'input_validator':None}

    def __init__(self, *args, **kwargs):
        for key, default in self.default_attributes.items():
            value = kwargs.pop(key, default)
            if 'No default provided.' == value: # When no default get, set to None
                value = default
            setattr(self, key, value)

class FormField():
    ordering_iterator = itertools.count()

    def __init__(self, **field_kwargs):
        self._ordering = next(self.ordering_iterator)
        self.name = ''
        self.field_kwargs = field_kwargs

    def as_widget(self, form = None, *args, **kwargs):

        # Determine if form is object form
        if form:
            obj_form = hasattr(form, 'obj')
        else:
            obj_form = False

        create_kwargs = copy.copy(self.field_kwargs)
        create_kwargs['form'] = form

        # Set the current form objects value as the current value
        if obj_form:

            # Value may be function of the form or the object related to the form.
            for value_source in [form, form.obj]:
                try:
                    create_kwargs['value'] = getattr(value_source, 'set_' + self.name)()
                    break
                except:
                    pass

        create_kwargs.update(kwargs)
        return create_field(self.name, **create_kwargs)

    def bind_name(self, name, *args, **kwargs):
        self.name = name

def create_field(name, **schema_kwargs):
    """ Return a field instance created from the given kwargs. """

    field_type_dict = {'string' : TextField, 'boolean': BooleanField,
        'password' : PasswordField, 'integer':IntegerField, 'date':DateField,
        'float':FloatField, 'related':RelatedField, 'email':EmailField,
        'decimal':FloatField, 'm2m':M2MRelatedField}
    field = None

    #Extract meta data
    extra_meta = schema_kwargs.pop('meta', {}) #Overrides any setting in meta
    meta_data = copy.copy(schema_kwargs)
    if 'verbose_name' not in extra_meta:
        extra_meta.update({'verbose_name':name})
    meta_data.update(extra_meta)
    field_meta = FieldMeta(**meta_data)

    schema_kwargs.update({'meta':field_meta})

    #Return instance of desired field
    try:
        if schema_kwargs['type'] == 'string':
            if field_meta.choices:
                field = ChoiceField(name, **schema_kwargs)
            else:
                field = TextField(name, **schema_kwargs)
        elif schema_kwargs['type'] == 'related':
            if isinstance(field_meta.related_schema, list):
                field = GenericRelatedField(name, **schema_kwargs)
            else:
                field = RelatedField(name, **schema_kwargs)
        else:
            FieldClass = field_type_dict[schema_kwargs['type']]
            field = FieldClass(name, **schema_kwargs)
    except:
        logging.error('RMS: Failed to create field.')
        logging.error(traceback.format_exc())

    return field


class GenericField(BoxLayout):
    color = ListProperty([0,0,0,0])
    displayed_name = StringProperty('')
    input_font_size = StringProperty('16dp') 
    input_height = ObjectProperty('40dp')   
    input_size_hint = ObjectProperty((0.5, 1))
    input_pos_hint = ObjectProperty({'center_x':0.5, 'center_y':0.5})
    is_valid = BooleanProperty(True)
    label_font_size = StringProperty('16dp')
    label_height = ObjectProperty('40dp') 
    label_size_hint = ObjectProperty((0.5,1))
    standard_height = '50dp'
    str_value = StringProperty('')
    value = ObjectProperty('')

    @property
    def invalid_animation(self):
        return Animation(color=[1,0,0,1], duration =0.2) + Animation(color=[0,0,0,0])

    def __init__(self, name, **kwargs):
        self.name = name
        self.app = App.get_running_app()
        self.verbose_name = self.name.title()
        self.nullable = False
        self.form = None
        self.hint_text = ''
        self.hide_label = False
        super().__init__()
        
        for attribute, value in kwargs.items():
            if attribute != 'value':
                setattr(self, attribute, value)

        self.set_initial_value(kwargs)
        self.value_to_str()
        self._set_field_label_text()

        if self.hide_label:
            if 'label' in self.ids:
                self.remove_widget(self.ids['label'])

        if self.meta.hidden: # Hide widget
            Clock.schedule_once(self.hide_widget, 0.05)
        else:
            self.height = self.standard_height

    @classmethod
    def add_input_widget(cls, instance, input_type, *args, **kwargs):

        #Determine the type of input to add
        if input_type == 'text_input':
            if instance.meta.readonly:
                input_widget = Label(text = instance.str_value,
                    size_hint = instance.input_size_hint)
                
            else:
                input_widget = TextInput(text = instance.str_value,
                    size_hint = instance.input_size_hint)

            #Set size and appearance of input and its text
            input_widget.font_size = instance.input_font_size

            if instance.input_height:
                input_widget.height = instance.input_height
            input_widget.hint_text = instance.hint_text

            # Bind a listener for when the text changes
            input_widget.bind(text=instance.on_text)

        elif input_type == 'date':

            if instance.meta.readonly:
                input_widget = Label(text = instance.str_value,
                    size_hint = instance.input_size_hint)
            else:
                input_widget = PrimaryButton(on_release = instance.show,
                    size_hint = instance.input_size_hint, text = instance.str_value)
                instance.bind(str_value = lambda instance, text_str: setattr(input_widget,
                    'text', text_str))

        if isinstance(input_widget, Label):
            instance.bind(str_value = input_widget.setter('text'))
            input_widget.font_color = instance.form.label_color

        # Set position of widget
        input_widget.pos_hint = instance.input_pos_hint
        # Add widget as an attribute as well as a child to the Field widget
        instance.input_widget = input_widget
        instance.add_widget(input_widget)

    @property
    def clean_value(self):
        """ Return cleaned value of field """
        if self.value == '':
            if self.type in ['date', 'datetime']:
                return None
        return self.value

    @property
    def serializable_value(self):
        """ Converts a value to JSON serializable format """

        if isinstance(self.value, datetime.datetime):
            for time_format in orm_settings.REMOTE_DATETIME_FORMAT:
                try:
                    return self.value.strftime(time_format)
                except:
                    pass
        elif isinstance(self.value, datetime.date):
            for time_format in orm_settings.REMOTE_DATE_FORMAT:
                try:
                    return self.value.strftime(time_format)
                except:
                    pass

        return self.value

    @property
    def value_is_null(self):
        return self.value in ['', 0, None]

    def _generate_field_label_text(self):
        name = self.meta.verbose_name.replace('_', ' ').title()
        if not self.nullable:
            name = f"{name}*"
        return f"[b]{name}[/b]"


    def _set_field_label_text(self):
        self.displayed_name = self._generate_field_label_text()



    def get_null_value(self, field_type, *args, **kwargs):
        if field_type in ['float', 'integer']:
            return 0
        elif field_type == 'm2m':
            return []
        return ''

    def hide_widget(self, *args):
        def recursive_hide(root_widget):
            root_widget.size_hint_y = None
            root_widget.height = 0
            root_widget.opacity = 0
            for c in root_widget.children:
                recursive_hide(c)
        recursive_hide(self)

    def on_input_height(self, input, value):
        if not isinstance(value, str):
            input.font_size = dp(value/1.7)

    def on_is_valid(self, field, value):
        if value == False:
            self.invalid_animation.start(self)

    
    def on_text(self, input, value):
        """ If an object is associated with the Field, update the value of the underlying
        object when the Field's value changes. """
        if self.form:
            if self.form.obj:
                setattr(self.form.obj, self.name, self.value)

    def on_value(self, widget, value):
        """ When the field's value changes, update the corresponding object. """

        if self.form:
            if self.form.obj and value:
                setattr(self.form.obj, self.name, value)
        self.value_to_str()

    def set_initial_value(self, init_kwargs):
        """ Determine the initial value to set for the field """

        if self.form:
            if self.form.has_obj and 'value' not in init_kwargs:
                try:
                    init_kwargs['value'] = getattr(self.form.obj, self.name)
                except AttributeError:
                    pass

        explicit_value = 'value' in init_kwargs
        if not explicit_value: 
            if self.meta.default != None:
                self.value = self.meta.default
            else:
                self.value = self.get_null_value(init_kwargs['type'])
        else:
            # Use supplied value
            value = init_kwargs['value']
            if not value:
                self.value = self.get_null_value(init_kwargs['type'])
            else:
                self.value = init_kwargs['value']

    def validate(self, *args, **kwargs):
        """ Make sure that the information entered is correct. """

        validator = kwargs.pop('validator', None)
        self.is_valid = True
        if not validator:
            if not self.nullable:
                if self.value == '' or self.value == None:
                    self.is_valid = False
        else:
            self.is_valid = validator(self.value)
        return self.is_valid

    def value_to_str(self):
        """ Convert the fields current value to a human readable string. """
        
        self.str_value = str(self.value)


class BooleanField(GenericField):
    
    def __init__(self, name, **kwargs):
        super().__init__(name, **kwargs)
        self.ids['check_box'].active = self.value
        self.ids['check_box'].bind(active = self.on_check_active)

    def on_check_active(self, checkbox, value, *args, **kwargs):
        self.value = value

    def validate(self, *args, **kwargs):
        """ Always valid """
        self.is_valid = True
        return self.is_valid


class TextField(GenericField):
    input_size_hint = ObjectProperty((0.5, 1))
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_input_widget(self, 'text_input')
        if self.meta.long_string:
            self.orientation = 'vertical'
            self.ids['label'].size_hint = (1, 0.3)
            self.input_widget.size_hint = (1, 0.7)
            self.height = self.height*3

        else:
            self.input_widget.multiline = False

    def on_text(self, input, value):
        self.value = value
        super().on_text(input, value)

class EmailField(TextField):
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

    def validate(self, *args, **kwargs):
        self.is_valid = True
        if not re.search(self.regex,self.value):
            self.is_valid = False
            return False
        return super().validate(*args, **kwargs)


class PasswordField(TextField):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = ''
        self.input_widget.password = True


class IntegerField(GenericField):
    input_size_hint = ObjectProperty((0.3, 1))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_input_widget(self, 'text_input')
        self.input_widget.font_size = '30dp'
        if not self.meta.readonly:
            self.input_widget.bind(focus = self.on_focus)
        self.input_widget.halign = 'right'
        self.input_widget.input_filter = 'int'

    def on_focus(self, widget, focus_state, *args, **kwargs):
        if not self.value and focus_state:
            self.input_widget.text = ''

    def on_text(self, input, value):
        if value == '':
            value = '0'
        self.value = self.text_to_value(value)
        super().on_text(input, value)

    def text_to_value(self, value):
        try:
            return int(value)
        except:
            return 0

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)

        # If previous validation checks failed, don't bother continuing
        if not self.is_valid:
            return

        # Check value exceeds min
        if self._requires_check('min'):
            if self.value < self.meta.min:
                self.is_valid = False

        # Check value is less than max
        if self._requires_check('max'):
            if self.value > self.meta.max:
                self.is_valid = False

        return self.is_valid

    def _requires_check(self, check_type):
        """ Determines whether a certain value check type will be required during field
        validation.

        check_type: String, can be either 'min' or 'max'
        """
        meta_value = getattr(self.meta, check_type)
        if meta_value in [None, False]:
            return False
        else:
            return True

class FloatField(IntegerField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.input_widget.input_filter = 'float'

    def text_to_value(self, value):
        return float(value)
        
class ChoiceField(GenericField):
    input_size_hint = ObjectProperty((0.5, 1))
    dropdown_btn_height = '50dp'
    build_dropdown_delay = 0.6

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.build_selection_widget()
        self.post_dropdown_build()

    def build_selection_widget(self, *args):
        """ Build the widget that will be used when making the selection of a choice """
        self.holder = BoxLayout(orientation = 'horizontal',
            size_hint = self.input_size_hint)
        self.add_widget(self.holder)
        self.trigger_button = PrimaryButton(text='',size_hint=(1,1))
        self.holder.add_widget(self.trigger_button)
        Clock.schedule_once(self._build_dropdown, self.build_dropdown_delay) #Delay resource heavy task

    @property
    def choice_list(self):
        return self.meta.choices
        
    def _build_dropdown(self, *args, **kwargs):
        """ Construct the Dropdown menu"""
        try:
            self.dropdown = DropDown()

            button_text = '-'
            
            for db_value, verbose_name in self.get_choice_list(*args, **kwargs):
                # If no default value is set and field can't be nullable, set the first
                # choice as the current value
                if self.value == db_value:                    
                    button_text = verbose_name

                # Set button text to match currently defined field value
                elif db_value == self.value:
                    button_text = verbose_name
                self.add_dropdown_entry(db_value, verbose_name)

            self.trigger_button.text = button_text
            if not self.meta.readonly:
                self.trigger_button.bind(on_release=self._open_dropdown)
            else:
                self.trigger_button.color = [1,1,1,1]
                self.trigger_button.background_normal = 'atlas://contrib/static/img/rmsatlas/inactive_button'
            self.dropdown.bind(on_select=self.set_value)
            
        except:
            logging.error('RMS: Failed to build drop down for choice field')
            logging.error(f"RMS: {traceback.format_exc()}")

    def _open_dropdown(self, *args, **kwargs):
        self.dropdown.open(self.trigger_button)



    def add_dropdown_entry(self, db_value, verbose_name, button_class = DropDownButton,
        *args, **kwargs):

        mark_as_selected = kwargs.pop('mark_as_selected', False)

        # Create the button for the new entry
        btn = button_class(text=verbose_name, size_hint_y=None,
            height=self.dropdown_btn_height)
        btn.db_value = db_value
        choice_tuple = (btn.db_value, btn.text)
        btn.bind(on_release=lambda btn: self.dropdown.select(choice_tuple))
        self.dropdown.add_widget(btn)

        if mark_as_selected:
            self.set_value(None, choice_tuple)

    def get_choice_list(self, *args, **kwargs):
        if self.meta.nullable:
            return list((('', '-'), *self.choice_list))
        else:
            return self.choice_list

    def post_dropdown_build(self, *args, **kwargs):
        """ Hook to perform certain actions before the dropdown is populated """
        pass


    def set_value(self, dropdown, choice_tuple):
        try:
            self.value = choice_tuple[0]
            self.trigger_button.text = choice_tuple[1]
        except ValueError:
            pass

class ClickableLabel(ButtonBehavior, Label):
    inner_padding = NumericProperty(5)
    shadow_offset_x = 2
    shadow_offset_y = -1
    animation_scaling_factor = 1.1
    shadow_color = ListProperty(hex_to_rgba("#000000", 0.5))
    background_color = ListProperty(hex_to_rgba("#ded073"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with self.canvas.before:
            Color(*self.shadow_color)
            self.shadow = Ellipse(size = self.size)

            Color(*self.background_color)
            self.circle = Ellipse(size = self.size)

        self.bind(pos = self.repos_canvas)
        self.bind(size = self.resize_canvas)

    def on_press(self, *args, **kwargs):
        original_size = copy.copy(self.size)
        new_size = (original_size[0]*self.animation_scaling_factor,
            original_size[1]*self.animation_scaling_factor)
        original_color = copy.copy(self.color)
        grow_animation = Animation(size=new_size, color = [1,1,1,1], duration = 0.1)
        fade_animation =  Animation(size = original_size, color = original_color, duration = 0.2)
        full_animation = grow_animation + fade_animation
        full_animation.start(self)

    def repos_canvas(self, instance, pos, *args, **kwargs):
        self.circle.pos = pos
        self.shadow.pos = pos[0] + self.shadow_offset_x, pos[1] + self.shadow_offset_y

    def resize_canvas(self, instance, size, *args, **kwargs):
        self.circle.size = size
        self.shadow.size = size

class RelatedField(ChoiceField):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.to_schema = self.app.store.model_registry.schema_for_related(
            related_schema = self.related_schema)
        self.to_model_name = self.to_schema['model_name']
        self.to_cls = self.app.store.model_registry.get_model_class(
            model_name = self.to_model_name)

    def get_queryset(self, *args, **kwargs):
        qs = self.app.store.query(self.to_model_name, **kwargs)
        if self.meta.queryset_filter:
            return self.meta.queryset_filter(qs, self)
        else:
            return qs

    def get_choice_list(self, index = 0, *args, **kwargs):
        qs = self.get_queryset(self.to_model_name, order_by = self.to_cls.verbose_name_field,
            **self.meta.queryset_filter)
        choice_list = []
        if self.nullable:
            choice_list.append((None, '-'))
        for obj in qs:
            choice_list.append((obj.resource_uri, str(obj)))
        return choice_list

    def on_press(self, *args, **kwargs):
        Clock.schedule_once(self._build_popup, 0)

    def post_dropdown_build(self, *args, **kwargs):
        """ Inserts a button to add another object. """
        inner = FloatLayout(size_hint = (None,1), width = '35dp')
        add_button = ClickableLabel(text = orm_settings.ADD_RELATED_TEXT(),
            markup = True, color = hex_to_rgba("#5d9158") ,
            pos_hint = {'center_x':0.5,'center_y':0.5}, on_release = self.on_press)
        inner.add_widget(add_button)
        self.holder.add_widget(inner)

    def _build_popup(self, *args, **kwargs):
        AddPopupClass = import_class(orm_settings.ADD_RELATED_POPUP_CLASS)
        popup = AddPopupClass(model_name = self.to_model_name, field = self)
        Clock.schedule_once(popup.open, 0.2)

class M2MRelatedField(RelatedField):
    input_font_size = StringProperty('25dp') 
    selected_count = NumericProperty(0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        
    def post_dropdown_build(self, *args, **kwargs):
        pass

    def on_selected_count(self, instance, value):
        self._set_field_label_text()

    def build_selection_widget(self, *args):
        Clock.schedule_once(self.add_m2m_widget, 0.1)

    def add_m2m_widget(self, *args):
        self.input_widget = SelectManyWidget(field_widget = self)
        self.add_widget(self.input_widget)

    def add_value(self, obj):
        self.value.append(obj.resource_uri)

    def remove_value(self, obj):
        try:
            self.value.remove(obj.resource_uri)
        except ValueError:
            pass

    def validate(self, *args, **kwargs):

        """ Always valid """
        if self.meta.min:
            if self.meta.min > len(self.value):
                self.is_valid = False
                Clock.schedule_once(self.reset_validation,0.2)

        return self.is_valid

    def reset_validation(self, *args):
        self.is_valid = True

    def _generate_field_label_text(self):
        text = super()._generate_field_label_text()
        return f"{text} ({self.selected_count})"
        

class GenericRelatedField(ChoiceField):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.to_schemas = []
        self.to_model_names = {}
        for s in self.related_schema:
            related_schema = self.app.store.model_registry.schema_for_related(
                related_schema = s)
            self.to_schemas.append(related_schema)
            self.to_model_names[related_schema['model_name']] = related_schema

    def get_choice_list(self, index = 0, *args, **kwargs):
        qs_list = []
        for model_name in self.to_model_names:
            model_qs = self.app.store.query(model_name, **kwargs)
            qs_list.append(model_qs)
            
        choice_list = []
        if self.nullable:
            choice_list.append((None, '-'))
        for qs in qs_list:
            for obj in qs:
                choice_list.append((obj.resource_uri, str(obj)))
        return choice_list

class DateField(GenericField):
    date_format = "%d/%m"
    DateWidgetClass = import_class(orm_settings.DATE_WIDGET)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_input_widget(self, 'date')

    def set_date(self, date):
        self.value = date
        if self.form:
            if self.form.obj:
                setattr(self.form.obj, self.name, self.value)
        self.value_to_str()

    def show(self, *args, **kwargs):
        #Don't pop up if the value is readonly
        widget = self.DateWidgetClass(on_close = self.set_date, current_date = self.value)
        widget.open(title = self.verbose_name)

    def validate(self, *args, **kwargs):

        """ Make sure that the information entered is correct. """
        self.is_valid = True
        if not self.nullable:
            if self.value == '':
                self.is_valid = False
        return self.is_valid

    def value_to_str(self):
        """ Convert the fields current value to a human readable string. """

        if self.value:
            if isinstance(self.value, str):
                for DateFormat in orm_settings.REMOTE_DATE_FORMAT:
                    try:
                        dt_object = datetime.datetime.strptime(self.value, DateFormat)
                        break
                    except:
                        pass
                new_str_value = datetime.datetime.strftime(dt_object, self.date_format)
            else: 
                new_str_value = datetime.datetime.strftime(self.value, self.date_format)
        else:
            new_str_value = '-'
        self.str_value = new_str_value