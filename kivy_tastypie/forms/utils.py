import importlib, inspect
import logging

from kivy_tastypie.config import import_class
from kivy_tastypie.config import orm_settings

form_class_registry = {}

def build_form_registry():
    """ Create a registry dict of 'model_name':'model_form_class' key:value pairs. 
    include special 'base_form_class' key for the Base class to use when no user defined
    class exists. """

    BaseFormClass = import_class(orm_settings.BASE_FORM_CLASS)
    form_class_registry['base_form_class'] = BaseFormClass

    #Iclude any user defined classes
    if orm_settings.FORM_DEFINITIONS:
        for name, cls in inspect.getmembers(importlib.import_module(orm_settings.FORM_DEFINITIONS),
            inspect.isclass):
            if cls != BaseFormClass and  issubclass(cls, BaseFormClass):
                form_class_registry[cls.get_registry_id()] = cls

    return form_class_registry

def get_model_form_class(form_type, model_name):
    #Search through registry to find forms that fit the bill
    if not form_class_registry:
        build_form_registry()
        
    class_id = f"{model_name}-{form_type}"
    for class_name, cls in form_class_registry.items():
        if hasattr(cls, 'model_name'):
            if cls.get_registry_id() == class_id:
                return cls
    return form_class_registry['base_form_class']