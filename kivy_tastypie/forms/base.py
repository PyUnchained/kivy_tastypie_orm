from copy import deepcopy
from functools import partial
import itertools
import logging 
import traceback

from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics import *
from kivy.lang import Builder
from kivy.properties import ListProperty, ObjectProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.stacklayout import StackLayout

from kivy_tastypie.forms.fields import (create_field, FieldGroup, GenericField, FormField,
    FieldLabel)
from kivy_tastypie.forms.layout import get_default_layout
from kivy_tastypie.uix.mixins import MatchChildHeightMixin
from kivy_tastypie.utils import Timer, timeit, write_to_log


Builder.load_string("""
<Form>:
    orientation: 'vertical'
    padding: [10,5]
""")

class Form(BoxLayout):
    auto_height = BooleanProperty(False)
    background_color = ListProperty([1,1,1,0])
    build_delay = 0
    disable_animation = BooleanProperty(False)
    label_color = ListProperty([1,0.2,0.2,1])
    layout_class = None
    prevent_scroll = BooleanProperty(False)
    primary_color = (0.4,0.5,1,1)
    secondary_color = ListProperty([0.5,0.5,0.5,1])
    tertiary_color = ListProperty([0.9234,0.345345,0.0258345,1])
    show_animation = Animation(opacity=1, duration = 0.1)
    valid_form_types = ['add', 'edit', 'view']

    def __init__(self, *args, **kwargs):
        self.built = False
        self.opacity = 0
        self.initial = kwargs.pop('initial', {})
        self.popup = kwargs.pop('popup', None)



        # Certain kwargs already have a default value set, but these must be removed
        # to prevent the BoxLayout class's __init__ method freaking out
        default_provided_kwrags = ['background_color', 'primary_color', 'secondary_color', 'tertiary_color']
        for attribute_name in default_provided_kwrags:
            setattr(self, attribute_name,
                kwargs.pop(attribute_name, getattr(self, attribute_name)))

        self.fields = []
        self.form_content = None
        self.app = App.get_running_app()
        self._configure_layout(init_kwargs = kwargs)
        super().__init__(**kwargs)
        Clock.schedule_once(self._build_form, self.build_delay)
        


    @classmethod
    def get_registry_id(cls):
        return f"{cls.model_name}-{cls.type}"

    @property
    def cleaned_data(self, *args, **kwargs):
        data = {}
        for f in self.field_widgets:
            data[f.name]=f.clean_value
        return data

    @property
    def field_proxies(self):
        proxy_list = []
        for attr in dir(self):
            if attr != 'field_proxies':
                f = self.get_proxy_field(attr)
                if f:
                    proxy_list.append(f)
        proxy_list.sort(key = lambda x : x._ordering)
        return proxy_list

    @property
    def field_widgets(self):
        """ Returns a list of all Field widgets that inherit from GenericField """
        return self.__extract_field_widgets(self.form_content)

    @property
    def has_obj(self):
        if hasattr(self, 'obj'):
            return self.obj != {}
        else:
            return False

    def after_fields(self, *args, **kwargs):
        """ Hook to handle events occuring after field widgets are created. """
        pass

    def before_fields(self, *args, **kwargs):
        """ Hook to handle events occuring before field widgets are created. """
        pass

    def get_field_widgets(self):
        return [x.as_widget(label_color = self.label_color) for x in self.field_proxies]


    def get_field(self, name):
        for f in self.field_widgets:
            if f.name == name:
                return f

    def get_proxy_field(self, name, *args):
        try:
            value = getattr(self, name)
            if issubclass(value.__class__, FormField):
                value.bind_name(name)
                return value
            else:
                return None
        except:
            return None

    def repos_canvas(self, instance, pos, *args, **kwargs):
        self.bg.pos = pos

    def resize_canvas(self, instance, size, *args, **kwargs):
        self.bg.size = size

    def style_widget(self, *args, **kwargs):
        with self.canvas.before:
            Color(*self.background_color)
            self.bg = Rectangle(size=self.size)

        self.bind(pos = self.repos_canvas)
        self.bind(size = self.resize_canvas)

    def validation_error_for_field(self, name):
        f = self.get_field(name)
        f.is_valid = False

    def __extract_field_widgets(self, root_widget, widgets_found = []):
        """ Identify and extract GenericField objects from the form's content """
        for f in root_widget.children:
            if issubclass(f.__class__, GenericField):
                widgets_found.append(f)
            elif isinstance(f, FieldGroup):
                self.__extract_field_widgets(f, widgets_found)
        return widgets_found

    def _build_form(self, *args, **kwargs):
        """
        Generates form field widgets based on the schema for the model obtained 
        from the api.
        """

        self._prepare_content_widget()

        try:
            self.before_fields()

            if self.sv not in self.children: # Already a child if form is built after updating existing obj
                self.add_widget(self.sv)

            
            field_widgets = self.get_field_widgets()
            if self.layout:
                field_widgets = self._coerce_to_layout(field_widgets)

            for f in field_widgets:
                self.form_content.add_widget(f)

            self.after_fields()
            self.style_widget()

            if self.prevent_scroll:
                self.sv.height = self.sv.height*1.1
            
            if self.popup or self.auto_height:
                Clock.schedule_once(self._compute_height, 0.1)

            self.show_animation.start(self)
            self.built = True
            
        except:
            logging.error("RMS: An error occured building the form.")
            logging.error(f"RMS: {traceback.format_exc()}")

    def _compute_height(self, *args, **kwargs):
        max_retry = 5
        if not 'attempts' in kwargs:
            kwargs['attempts'] = 0

        attempts = kwargs['attempts']

        # Depending on the scheduling of events, this function may end up being called before
        # the form has been added to a parent widget. In that case, wait a while then try again
        try:
            self.parent.height
        except AttributeError:
            attempts += 1
            if attempts < max_retry:
                Clock.schedule_once(partial(self._compute_height, attempts = attempts), 0.1)
            else:
                write_to_log(f'Failed to calculate height of form after {attempts} tries',
                    level = 'warning')

        total_height = 0 # Add a bit of padding to the bottom of the form
        for c in self.form_content.children:
            total_height += c.height
        smallest = min(total_height, self.parent.height)

        if not self.disable_animation:
            # Play animation revealing the form
            anim = Animation(height=smallest + self.height, duration = 0.3, t = 'in_out_cubic')
            anim.start(self)
        else:
            # Simply match height without animation
            self.height += smallest

    def _configure_layout(self, init_kwargs = {}, *args, **kwargs):
        """ Configures the form's layout based on any presets defined in layouts.py """

        # Determine the layout for the form
        if self.layout_class:
            self.layout = self.layout_class()
        else:
            self.layout = init_kwargs.pop('layout', None)

        # Check that this is not possibly a model form
        if not self.layout:
            try:
                self.layout = get_default_layout(self.model_name, self.form_type)
            except:
                pass

        if self.layout:
            self.layout.form = self

    def _prepare_content_widget(self, *args, **kwargs):
        """ Generates a widget that will contain the FormField widgets. """

        #If the form is being reset after the data has been updated these widgets
        #already exist and need not be created again.
        if not self.form_content:
            self.form_content = GridLayout(cols=1, spacing=10, size_hint_y=None)
            self.form_content.bind(minimum_height=self.form_content.setter('height'))
            self.sv = ScrollView(size_hint=(1, 1), always_overscroll = False, bar_width = 0)
            self.sv.add_widget(self.form_content)



class GenericModelForm(Form):
    obj = ObjectProperty({})

    def __init__(self, form_type, *args, **kwargs):
        try:
            self.model_name = kwargs['obj'].model_name
            kwargs.pop('model_name', None) # Removed, since BoxLayout's __init__ doesn't expect it
        except (AttributeError, KeyError):
            self.model_name = kwargs.pop('model_name')

        self.form_type = form_type
        self.post_save = kwargs.pop('post_save', {})
        super().__init__(**kwargs)

    @property
    def is_valid(self):
        try:
            resp = True
            for element in self.form_content.children:
                if hasattr(element, "validate"):
                    element_valid = element.validate()
                    if not element_valid:
                        resp = False

            #Perform any extra validation specified by the layout
            if resp:
                resp = self.layout.on_validate(self)
            return resp
        except:
            
            logging.error("RMS: An error occured validating the form.")
            logging.error(f"RMS: {traceback.format_exc()}")
            return False




    @property
    def verbose_model_name(self):
        if self.obj:
            name = self.obj.model_name
        elif self.model_name:
            name = self.model_name
        else:
            return 'Model'
        return name.replace('_', ' ').title()


    def get_field_widgets(self):
        field_widgets = []
        schema = self.app.store.model_registry.get_schema(self.model_name)

        #If no fields are explicitly given, display all the fields eexcept the hidden
        #fields
        for name in self.layout.fields:
            # First check if the field is defined in the Form class definition
            proxy_field = self.get_proxy_field(name)
            if proxy_field:
                field_widgets.append(proxy_field.as_widget(form = self))
                continue

            field_kwargs = schema['fields'][name]                
            field_kwargs['form'] = self
            field_kwargs['label_color'] = self.label_color

            if name in self.layout.field_meta:
                field_kwargs.update(self.layout.field_meta[name])

            if self.obj:
                field_kwargs['value'] = getattr(self.obj, name)

            if name in self.initial:
                field_kwargs['value'] = self.initial[name]
                
            f = create_field(name, **field_kwargs)
            if f:
                field_widgets.append(f)

        return field_widgets

    def on_obj_updated(self, new_obj, *args, **kwargs):
        """ Update the form in place with a new object. """
        pass

    def on_save_error(self, *args, **kwargs):
        """ Hook to handle events when there is an error saving an object. """
        pass

    def on_save_success(self, obj, created = False, *args, **kwargs):
        """ Hook to handle events when there is an error saving an object. """
        if self.popup:
            self.popup.dismiss()
        if self.post_save:
            self.post_save(obj)


    def save(self, *args, **kwargs):
        if not self.is_valid: # Don't save if form invalid
            return False

        obj = None
        try:
            if self.obj:    # Object already existed, update it
                created = False
                self.obj.save()
                self.on_obj_updated(self.obj)
                obj = self.obj
            else:           # New object was created
                created = True
                obj = self._create_obj()
                
            self.on_save_success(obj, created = created)
            return True

        except:
            write_to_log('Error occured in form save proccess',
                level = 'error')
            self.on_save_error()
            return False

    def __coerce_field_for_group(self, field, x_hint):
        """ Change layout instructions for a field so that it is neatly
        displayed inside the FieldGroup widget. """

        field.orientation = 'vertical'
        field.size_hint = (x_hint,1)

        for c in field.children:
            c.size_hint = (1,None)
            if not issubclass(c.__class__, FieldLabel):
                c.height = '50dp'
            else:
                c.height = '20dp'
        field.ids['label'].halign = 'center'
        return field

    def __lay_out_group(self, group_fields, field_widgets):
        form_group = FieldGroup(orientation = 'horizontal')
        x_hint = 1/len(group_fields)

        for field_name in group_fields:
            for f in field_widgets:
                if f.name == field_name:
                    f = self.__coerce_field_for_group(f, x_hint)
                    f.padding = [1,0]
                    form_group.add_widget(f)
                    field_widgets.remove(f)

        form_group.name = name = '_'.join(group_fields) + "_form_group"
        return form_group, field_widgets

    def _coerce_to_layout(self, field_widgets):
        """ Groups and lays out fields as defined by the field_layout class
        attribute."""
        if not self.layout.field_layout:
            return field_widgets

        ordered_field_widgets = []
        for layout_element in self.layout.field_layout:
            if isinstance(layout_element, tuple):
                form_group, field_widgets = self.__lay_out_group(layout_element,
                    field_widgets)
                ordered_field_widgets.append(form_group)
            else:
                for f in field_widgets:
                    if f.name == layout_element:
                        ordered_field_widgets.append(f)
                        field_widgets.remove(f)
        return ordered_field_widgets


    def _create_obj(self, *args, **kwargs):
        """ Create an instance of the Model this form represents """
        ModelClass = self.app.store.model_registry.get_model_class(self.model_name)
        obj_kwargs = {}
        for f in self.field_widgets:
            obj_kwargs[f.name] = f.value
        obj = ModelClass(obj_kwargs, create = True)
        return obj

    def _delete(self, *args, **kwargs):
        if self.obj:
            self.obj.delete()



