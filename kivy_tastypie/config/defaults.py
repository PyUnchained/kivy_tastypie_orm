from kivy.utils import platform

# Remote Server Settings
if platform != 'android':
    REMOTE_URL = 'http://127.0.0.1:8100'
else:
    REMOTE_URL = None
    
REMOTE_DATE_FORMAT = ["%Y-%m-%d", "%d-%m-%Y", "%d-%m"]
REMOTE_DATETIME_FORMAT = ["%Y-%m-%dT%H:%M:%S.%f", "%Y-%m-%dT%H:%M:%S"]
API_ROOT = None

# User defined Classes
LAYOUT_DEFINITIONS = None
MODEL_DEFINITIONS = None
FORM_DEFINITIONS = None

# UIX Elements
PRIMARY_BUTTON_CLASS = 'kivy.uix.button.Button'
DROPDOWN_BUTTON_CLASS = 'kivy.uix.button.Button'
ADD_RELATED_POPUP_CLASS = None  # Needs a default
BASE_FORM_CLASS = 'kivy_tastypie.forms.GenericModelForm'
DATE_WIDGET = 'kivy_tastypie.uix.date.DateWidget'  # Needs a default
