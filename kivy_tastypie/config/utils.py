import importlib
import logging
import os
import pathlib

from kivy_tastypie.errors import ConfigError

def import_class(import_string):
    path_elements = import_string.split('.')
    class_name = path_elements[-1]
    module_path = '.'.join(path_elements[:-1])
    module = importlib.import_module(module_path)
    return getattr(module, class_name)

class ConfigObject():
    """ Respresents the current settings specified for the kivy_tastypie package. """
    
    def __init__(self, user_defined_settings = os.getenv('ORM_SETTINGS', None), *args, **kwargs):
        #Find default settings module
        self.user_defined_settings = user_defined_settings
        settings_modules = []
        settings_modules.append(importlib.import_module('kivy_tastypie.config.defaults'))

        #Find custom defined settings module
        if not self.user_defined_settings:
            raise ConfigError('Please set "ORM_SETTINGS" environment variable to match the import path '
                f'for your settings file. E.g. "path.to.my.settings_file.py" ')

        if self.user_defined_settings:
            settings_modules.append(
                importlib.import_module(self.user_defined_settings))

        #Set attributes depending on the contents of the settings modules,
        #overriding any that are in defaults
        for module in settings_modules:
            for var in dir(module):
                if var.isupper():
                    setattr(self, var, getattr(module, var))

        if self.MODEL_DEFINITIONS == None:
            raise ConfigError('Please define path to module containing Model class definitions '
                f'in orm settings module. Current settings config file path: "{self.user_defined_settings}"')

    def update_setting(self, setting_name, value):
        setattr(self, setting_name, value)

orm_settings  = ConfigObject()