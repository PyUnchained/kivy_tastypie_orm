from functools import partial

from kivy.clock import Clock

class Queryset():
    """ Contains a list of objects returned as the result of a query to the DB. """

    def __init__(self, store, model_name, objects, *args, **kwargs):
        """ Create a queryset based off of the data returned from the DB. """
        self.model_name = model_name
        self.store = store
        self.objects = objects
        self.size = len(self.objects)
        self.model_class = self.store.model_registry.get_model_class(self.model_name)
        self.schema = self.store.model_registry.get_schema(self.model_name)


    def __iter__(self, *args, **kwargs):
        for x in self.objects:
            yield self.wrap_object(x, self.schema)

    def __getitem__(self, slice_index):
        return self.wrap_object(self.objects[slice_index], self.schema)

    def wrap_object(self, obj, schema):
        """ Creates a Model instance from the raw Python dictionary. """
        instance = self.model_class(obj, schema = schema)
        Clock.schedule_once(partial(self.bind_update_handler,instance), 1)
        return instance

    def bind_update_handler(self, obj, *args):
        self.store.bind(updated_model = obj.on_update)
