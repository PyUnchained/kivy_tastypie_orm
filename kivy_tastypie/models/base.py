import datetime
import random
import traceback
import functools

from kivy.app import App
from kivy.logger import Logger
from kivy.event import EventDispatcher
from kivy.clock import Clock

from kivy_tastypie.config import orm_settings
from kivy_tastypie.http_requests import secure_post, secure_get, join_api_path
from kivy_tastypie.utils import write_to_log

class BaseModel(EventDispatcher):
    model_name = ''
    verbose_name_field = 'id'

    def __init__(self, obj, *args, **kwargs):
        super().__init__()
        self.register_event_type('on_db_refresh')
        self._schema = kwargs.pop('schema',
            self.app.store.model_registry.get_schema(self.model_name))
        
        self._obj = obj
        
        create = kwargs.pop('create', False)
        self.create_silently = kwargs.pop('create_silently', False)
        if create:
            self.create()
        

    def __str__(self, *args, **kwargs):
        return str(self._obj[self.verbose_name_field])

    def __setattr__(self, name, value):
        """ Makes sure that when the BaseModel attributes are set, the corresponding value
        is set in the dict representing the object."""
        try:
            super().__setattr__(name, value)
        except AttributeError:
            write_to_log(f'Failed to set attribute "{name}". Make sure that the '
                f'{self.__class__} class does not declare a method with the same name.',
                level = 'error')
        #Whenever the _obj attribute is set, make sure the Model object's attributes are
        #reset along with it
        if name == '_obj':
            self.__set_attributes()


    @classmethod
    def already_exists(cls, data):
        db_record = self.app.store.get(model_name)
        if db_record and 'objects' in db_record:
            other_objects = db_record['objects']

    @property
    def api_compatible_dict(self):
        """ Reurns the object as  a dict that will be suitable for use when communicating
        with the remote API """
        resp = {}
        for field_name, field_schema in self._schema['fields'].items():
            resp[field_name] = self._to_json(getattr(self, field_name), field_name, field_schema)
        return resp

    @property
    def app(self, *args, **kwargs):
        return App.get_running_app()

    @property
    def exists_in_db(self):
        if 'resource_uri' in self._obj:
            if 'tmp' not in self._obj['resource_uri']:
                return True
        return False

    def on_db_refresh(self, *args):
        pass

    def resfresh_from_db(self):
        """ Update this model instance with the latest data contained in the db. """

        try:
            # Locate DB record
            object_list = self.app.store.get(self.model_name)['objects']
            for entry in object_list:
                if entry['resource_uri'] == self.resource_uri:
                    self._obj = entry
                    # Sent out the 'on_db_refresh' event
                    Clock.schedule_once(functools.partial(self.dispatch, 'on_db_refresh'),0.05)
                    break
        except:

            write_to_log('Error refreshing db', level = 'error')

    def __set_attributes(self):
        """ Sets the BaseModel object's attributes. Includes all fields listed in the schema.
        Where possible, the schema for the field will be used to convert the value from
        JSON format (usually a string) to the correct Python class (date, related foreign object,
        etc)
        """

        #Copy, as is, attributes found in JSON retreived from API not explicitly defined in schema
        if self._obj:
            ignored_attributes = list(self._schema['fields'].keys())
            for attribute_name, value in self._obj.items():
                if attribute_name not in ignored_attributes:
                    try:
                        setattr(self, attribute_name, value)
                    #Usually happens if the attribute name conlicts with a pre-defined class
                    #method
                    except AttributeError:
                        pass

        #Copy attributes defined in schema, converting them to the correct
        #Python type.
        for field_name, field_schema in self._schema['fields'].items():
            value = None

            #If there's an underlying model object, set the attribute to its
            #value
            if self._obj:
                try:
                    value = self._obj[field_name]
                except KeyError:
                    pass

            sanitized_value = self._to_python(value,field_name, field_schema)
            setattr(self, field_name, sanitized_value)


    def _get_create_post_data(self, *args, **kwargs):
        """ Creates a dictionary with all the information required to make a
        POST request. """
        data = self.api_compatible_dict
        data.pop('id', None)

        # Any keys whose value is None should be removed
        keys_to_remove = []
        for k, v in data.items():
            if v == None:
                keys_to_remove.append(k)
        for k in keys_to_remove:
            data.pop(k)
        return data

    def _parse_error(self, resp):
        error_pieces = []
        for key in ['error_message', 'error']:
            if key in resp:
                error_pieces.append(resp[key])
        return '\n'.join(error_pieces)

    def _to_json(self, value, field_name, field_schema):
        """ Converts a value to JSON serializable format """

        if isinstance(value, datetime.datetime):
            for time_format in orm_settings.REMOTE_DATETIME_FORMAT:
                try:
                    return value.strftime(time_format)
                except:
                    pass
        elif isinstance(value, datetime.date):
            for time_format in orm_settings.REMOTE_DATE_FORMAT:
                try:
                    return value.strftime(time_format)
                except:
                    pass
        return value


    def _to_null_value(self, field_name, field_schema):
        null_value = None
        if field_name == 'id':
            return null_value

        if field_name == 'created':
            'Null Created Value'
            return datetime.datetime.now() 

            
        if field_schema['type'] in ['float', 'integer']:
            null_value = 0

        return null_value

    def _to_python(self, value, field_name, field_schema):
        if not value:
            return self._to_null_value(field_name, field_schema)

        if field_schema['type'] == 'datetime':
            for format in orm_settings.REMOTE_DATETIME_FORMAT:
                try:
                    return datetime.datetime.strptime(value, format)
                except :
                    pass
        elif field_schema['type'] == 'date':
            for format in orm_settings.REMOTE_DATE_FORMAT:
                try:
                    return datetime.datetime.strptime(value, format).date()
                except:
                    pass
        return value

    def commit(self, *args, **kwargs):
        """ Adds the object currently associated with the ModelClass to the database. """
        local_db = self.app.store.get(self.model_name)
        if 'objects' in local_db:
            local_db['objects'].append(self.api_compatible_dict)
            self.app.store.store_sync()
            return True
        return False

    def create(self, *args, **kwargs):
        """ Create a record of this instance on the remote server. """
        verbose_model_name = self.model_name.replace('_', ' ').title()
        data = self._get_create_post_data()
        post_url = join_api_path(self.app.store.api_root, self.model_name)
        success, resp = secure_post(post_url, data)
        if not success:
            error_text = self._parse_error(resp)
            if 'violates unique constraint' in error_text:
                self.on_unique_contraint_failed()
            self.on_create_fail(error_text)
            return False
        else:
            self._obj = resp
            self.app.store.add_or_update_obj(self)
            self.on_create_success(obj = self)
        return True

    def delete(self, *args, **kwargs):
        okay = self.app.store.delete(self)
        if okay:
            self.on_delete_success()
        else:
            self.on_delete_fail()


            

    def on_create_fail(self, error_text, *args, **kwargs):
        write_to_log(f'Failed to create object!\n{error_text}', level = 'error')

    def on_create_success(self, *args, **kwargs):
        pass

    def on_delete_fail(self, *args):
        pass

    def on_delete_success(self, *args):
        pass

    def on_unique_contraint_failed(self, *args, **kwargs):
        pass

    def on_update(self, store, updated_model):
        """ When a record in the DB is updated, this listener checks to see if the updated
        model in question corresponds to this object. If it does, the object's attributes
        are updated """
        
        if self._obj['resource_uri'] == updated_model['resource_uri']:
            self._obj = updated_model

    def on_update_fail(self, error_text, *args):
        write_to_log('Failed to update object!',
            level = f'{error_text}')

    def on_update_success(self, obj, *args):
        pass

    def remote_sync(self, *args, **kwargs):
        """ Synchronize object data with the record stored on the remote server. """
        
        success, resp = secure_get(self.resource_uri)
        if success:
            self._obj = resp
            self.app.store.add_or_update_obj(self)
        return success

    def save(self, *args, **kwargs):
        if self.exists_in_db:
            _saved = self.update()
        else:
            _saved =self.create()
            
        if _saved:
            self.resfresh_from_db()

    def update(self, *args, **kwargs):
        obj_data = kwargs.pop('obj_data', self.api_compatible_dict)
        success, resp = secure_post(f"/core_api/{self.model_name}/", obj_data)
        if success:
            self._obj = resp
            self.app.store.add_or_update_obj(self)
            self.on_update_success(self._obj)
        else:
            error_text = self._parse_error(resp)
            self.on_update_fail(error_text)
            return False
        return success
