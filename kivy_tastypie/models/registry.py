from copy import deepcopy
from functools import partial
import traceback
import importlib, inspect

from kivy.logger import Logger

from kivy_tastypie.config import orm_settings
from kivy_tastypie.http_requests import async_get, join_api_path, fetch_all_schema, fetch_all_data
from kivy_tastypie.models.base import BaseModel

class ModelRegistry():

    db_write_delay = 0.15

    def __init__(self, store, *args, **kwargs):
        self.store = store
        self.registry = {}
        self.schema = {}
        self.__populate_registry()

    def add(self, cls, model_name):
        self.registry[model_name] = cls

    def get_model_class(self, model_name):
        for entry_name, cls  in self.registry.items():
            if entry_name == model_name:
                return cls
        Logger.warning("RMS: No Model class definition found for: "
            f"{model_name} ")

    def get_schema(self, resource_name, *args, **kwargs):
        try:
            return deepcopy(self.schema[resource_name])
        except:
            raise ValueError(f'Schema for resource "{resource_name}" not found.')

    def schema_for_related(self, related_schema):
        for model_name, val in self.schema.items():
            if model_name in related_schema:
                return val
        Logger.warning("RMS: Failed to find model name for related schema "
            f"{related_schema} ")

    def set_schema(self, schema = {}, *args, **kwargs):
        """ Set given model schema or set the schema saved in local DB."""
        if not schema:
            schema = self.store.get('schema') # Set existing
        else:
            self.store.put('schema', schema) # Save newly specified schema
        self.schema = schema

    def __populate_registry(self, schema = {}, *args, **kwargs):
        """ Finds all Model definitions and adds them to the registry. """

        # Import classes representing Models
        if not self.registry:
            for name, cls in inspect.getmembers(importlib.import_module(orm_settings.MODEL_DEFINITIONS),
                inspect.isclass):
                if cls != BaseModel and  issubclass(cls, BaseModel):
                    self.registry[cls.model_name] = cls

        #Set schema and models to synch
        self.set_schema(schema = schema)
