from collections import deque
from copy import copy, deepcopy
from functools import partial
from itertools import chain
from json.decoder import JSONDecodeError
from sys import getsizeof, stderr
import asyncio
import datetime
import json
import random
import re
import requests
import threading
import traceback

try:
    from reprlib import repr
except ImportError:
    pass
from natsort import natsorted

from kivy.clock import Clock
from kivy.properties import ObjectProperty
from kivy.logger import Logger
from kivy.event import EventDispatcher

from kivy_tastypie.config import orm_settings
from kivy_tastypie.forms.utils import build_form_registry
from kivy_tastypie.http_requests import (fetch_data_batch, async_get, join_api_path, fetch_all_schema,
    post_new_model, secure_delete)
from kivy_tastypie.http_requests.utils import get_request_string
from kivy_tastypie.models.queryset import Queryset
from kivy_tastypie.models.registry import ModelRegistry
from kivy_tastypie.utils import write_to_log, Timer, background_thread, timeit

class TastyPieMemStore(EventDispatcher):

    remote_url = orm_settings.REMOTE_URL
    delay_timer = 1
    updated_model = ObjectProperty({})

    def __init__(self, filename, api_root, *args, **kwargs):
        """
        schema - Latest schema detailing structure of all models exposed through API
        """
        super().__init__()
        self._data = {}
        self._is_changed = False
        self.indent = kwargs.pop('indent', 4)
        self.api_root = api_root
        self.filename = filename
        self.store_load()
        self.model_registry = ModelRegistry(self)
        self.tmp_filename = f"tmp_{self.filename}"
        build_form_registry()
        self.schema_configuration()

    @background_thread
    def store_sync(self, force = False):
        if not self._is_changed and not force:
            return

        try:
            json.dumps(
                self._data,
                indent=self.indent
            )

            with open(self.filename, 'w') as fd:                
                json.dump(
                    self._data, fd,
                    indent=self.indent
                )
            self._is_changed = False

        except:
            write_to_log("Kivy Tastypie: Failed to write DB to file.",
                level = 'error')
            self.debug_json(self._data)

    @property
    def api_base_url(self):
        return join_api_path(self.remote_url, self.api_root)
    
    @property
    def data_user(self):
        print (self.logged_in_user)

    @property
    def logged_in_user(self):
        return self.get('logged_in_user')

    def add_many(self, model_name, dict_list):
        data = self.get(model_name)
        data['objects'].extend(dict_list)
        self.put(model_name, data)
        if dict_list:
            self.refresh_related_objects(self.query(model_name,
                resource_uri=dict_list[0]['resource_uri'])[0])
            self.store_sync(force = True)

    def add_object(self, object_class, new_obj, *args, **kwargs):
        data = self.get(object_class)
        new_data = data
        new_data['objects'].append(new_obj)
        self.put(object_class, **new_data)

    def add_or_update_obj(self, obj):
        obj_list = self._data[obj.model_name]['objects']

        #Search through the list of all existing objects and replace the old record
        #with the new if found
        in_list = False
        for index, item in enumerate(obj_list):
            if item['resource_uri'] == obj.resource_uri:
                obj_list[index] = obj._obj
                in_list = True
                break

        #Add object if it was not previously found
        if not in_list:
            obj_list.append(obj._obj)

        self.refresh_related_objects(obj)
        self.store_sync(force = True)

    def clear(self):
        self._data = {}
        self._is_changed = True
        self.store_sync()

    def create_object(self, model_name, data):
        """
        Creates a new object locally and on the remote server. If the remote server
        can't be reached, the object is stored locally but with no resource_uri field.
        """

        #Make POST request
        data = self._pre_create(model_name, data)
        success, resp = post_new_model(model_name, data)
        
        #Save the record returned from remote in the DB
        if success:
            new_object = resp

        #If POST failed because the object already exists, don't bother saving anything
        #to the local DB
        elif 'error_message' in resp:
            if 'violates unique constraint' in resp['error_message']:
                return

        #If the POST failed for any other reason, save a temporary version of the object to
        #the local DB
        else:
            tmp_id = random.uniform(0,10)
            data['resource_uri'] = f'tmp/{tmp_id}'
            new_object = data

        objects_in_db = self.get(model_name)['objects']
        objects_in_db.append(new_object)
        self.store_sync()

    def debug_json(self, data, current_ident = 0, parent_obj = None):
        """ Debug JSON in the event of some failure saving it to the DB (usually
        results from non-serializable data in the dictionary). """
        ignored_types = [str, bool, list, float, int]
        spacing = ' '*current_ident
        for key, value in data.items():
            if isinstance(value, dict):
                self.debug_json(value, current_ident = current_ident + 1,
                    parent_obj = data)
            else:
                obj_type = type(value)
                if obj_type.__class__.__name__ == 'WidgetMetaclass':
                    print (value, '\n\n',obj_type)

    def delete(self, obj):

        try:
            db_records = self._data[obj.model_name]
            # Search through object list to find specified object
            for index, entry in enumerate(db_records['objects']):
                if entry['resource_uri'] == obj.resource_uri:
                    okay, resp = secure_delete(obj.resource_uri)
                    if okay:
                        db_records['objects'].pop(index) # Remove it
                        self.store_sync(force = True)
                    break
            return True

        except:
            write_to_log('Error occured deleting record', level = 'error')
            return False

    def fetch_all_data(self, *args, **kwargs):
        """ Retrieve all data stored on the remote server. Assumes that the schema and 
        list of models to keep in sync has already been retrieved. """
        user = self.logged_in_user
        username = user['username']
        self.error_count = 0

        #Determine all the list endpoints that will need to be hit
        get_parameters = self.fetch_all_data_get_args(user)
        endpoint_list = []
        api_info = self.model_registry.get_schema('api_info')
        get_query = get_request_string(get_parameters)
        for resource_name in self.fetch_all_data_resource_list(api_info):
            endpoint_options = api_info[resource_name]
            endpoint_query = join_api_path(self.remote_url,
                endpoint_options['list_endpoint'], get_query)
            endpoint_list.append((resource_name, endpoint_query))

        #Include and user defined endpoints t
        extra_endpoints = self.fetch_all_data_include_endpoints()
        for endpoint in extra_endpoints:
            endpoint_list.append(endpoint)
        
        #Synchronously make requests for all of the data
        remote_data = asyncio.run(fetch_data_batch(endpoint_list))
        objects_received_count = 0
        for model_data in remote_data:
            try:
                objects_received_count += len(model_data['objects'])
                self.put(model_data['meta']['model_name'], model_data)
            except:

                self.error_count += 1
                write_to_log(f'Failed to correctly retrieve data.\n\n{model_data}',
                    level = 'error')

        if objects_received_count:
            self._set_last_synch_time()

        return objects_received_count

    def fetch_all_data_get_args(self, user, *args, **kwargs):
        return []

    def fetch_all_data_include_endpoints(self, *args, **kwargs):
        return []

    def fetch_all_data_resource_list(self, api_info, *args, **kwargs):
        return list(api_info.keys())

    def get(self, key, *args, **kwargs):
        try:
            return self._data[key]
        except KeyError:
            return {}

    def get_object(self, model_name, **query_kwargs):
        qs = self.query(model_name, **query_kwargs)
        if qs.objects:
            return qs.objects[0]
        else:
            return None

    def get_related_objects(self, obj, *args):
        """ Find all records in db that have a field that appears to reference the target
        object. """

        related_fields = []
        related_uris = []

        # Get all foreign key field names
        for name, field_schema in obj._schema['fields'].items():
            if field_schema['type'] in ['related', 'm2m']:
                related_fields.append(name)

        # Get the URI for any objects related to this one by foreign keys
        for f in related_fields:
            uri = getattr(obj, f)
            if uri:
                if isinstance(uri, list): # For many-to-many fields the uris are provided as a list
                    for entry in uri:
                        related_uris.append((self._model_name_from_uri(entry), entry))

                # Other relations are one-to-one, so are only a string
                else:
                    related_uris.append((self._model_name_from_uri(uri), uri))

        # Find other models in the schema that have a fk pointing back to the
        # target obj's model class [via the 'related_schema_name']
        related_model_fields = []
        for model_name, model_schema in self.get('schema').items():
            try:
                for field_name, field_schema in model_schema['fields'].items():
                    if field_schema['type'] == 'related':
                        if obj._schema['related_schema_name'] in field_schema['related_schema']:
                            related_model_fields.append([model_name, field_name])
                            break
            except:
                pass
        
        # Find any specific instances of related objects that explicitly reference
        # the target obj and add their uris to the list
        for entry in related_model_fields:
            query_kwargs = {entry[1]:obj.resource_uri}
            qs = self.query(entry[0],  **query_kwargs)
            for instance in qs:
                if (instance.model_name, instance.resource_uri) not in related_uris:
                    related_uris.append((instance.model_name, instance.resource_uri))
        return related_uris

    def initial_sync_to_remote(self, logged_in_user = {}, *args, **kwargs):
        """ Perform the initial setup to synchronize local DB with the remote server's. """
        try:
            synch_start = datetime.datetime.now().replace(microsecond=0)
            self.__fetch_api_schema()
            objects_received_count = self.fetch_all_data(user = logged_in_user)
            synch_end = datetime.datetime.now().replace(microsecond=0)
            duration = str(synch_end - synch_start)
            db_size = self.mem_size()
            if self.error_count:
                level = 'warning'
            else:
                level = 'info'
            write_to_log(f"Kivy Tastypie: Sync to Remote Complete. {objects_received_count} records in "
                f"{duration} time. {db_size:.2f}Mb. {self.error_count} errors.", level = level)
            self.store_sync()
            return True
        except:
            write_to_log("Kivy Tastypie: An error occured during sync with remote server",
                level = 'error')
            return False

    def load(self):
        try:
            super().store_load()

        #Occurs if the databse has been corrupted in some way. To deal with it,
        #simply set the database to an empty dictionary.
        except JSONDecodeError:
            self.clear()
            super().store_load()

    def login(self, user_dict, *args, **kwargs):
        """ Register the currently logged in user and get their data """

        try:
            self.put('logged_in_user', user_dict)
            self.initial_sync_to_remote(user_dict)
            return True
        except:
            msg = f"""Kivy Tastypie: An error occured logging in. Data: 
                {user_dict}"""
            write_to_log(msg)
            return False

    def mem_size(self, handlers={}, units = 'mb', verbose=False):
        """ Returns the approximate memory footprint an object and all of its contents.

        Automatically finds the contents of the following builtin containers and
        their subclasses:  tuple, list, deque, dict, set and frozenset.
        To search other containers, add handlers to iterate over their contents:

            handlers = {SomeContainerClass: iter,
                        OtherContainerClass: OtherContainerClass.get_elements}

        """
        conversion_factor = {'kb':3, 'mb':6, 'b':0}
        dict_handler = lambda d: chain.from_iterable(d.items())
        all_handlers = {tuple: iter,
                        list: iter,
                        deque: iter,
                        dict: dict_handler,
                        set: iter,
                        frozenset: iter,
                       }
        all_handlers.update(handlers)     # user handlers take precedence
        seen = set()                      # track which object id's have already been seen
        default_size = getsizeof(0)       # estimate sizeof object without __sizeof__

        def sizeof(o):
            if id(o) in seen:       # do not double count the same object
                return 0
            seen.add(id(o))
            s = getsizeof(o, default_size)

            if verbose:
                print(s, type(o), repr(o), file=stderr)

            for typ, handler in all_handlers.items():
                if isinstance(o, typ):
                    s += sum(map(sizeof, handler(o)))
                    break
            return s

        return sizeof(self._data)/(10**conversion_factor[units])

    def model_name_from_data(self, data):
        """ Determine the model a particular dictionary represents based on the names of
        the fields present in the dictionary. """

        full_schema = self.get('schema')
        possible_models = list(full_schema.keys())

        # Essentially, search through all the schemas in the db. If any of the fields
        # don't match the keys in the data, remove the model from the list of possible
        # ones that match the data
        for model_name, model_schema in full_schema.items():
            try:
                for field_name, field_schema in model_schema['fields'].items():
                    if field_name not in data:
                        possible_models.remove(model_name)
                        break
            except:
                possible_models.remove(model_name)

        if len(possible_models) == 1:
            return possible_models[0]
        return None

    def put(self, key, data, *args, **kwargs):
        try:
            self._data[key] = data
            self._is_changed = True
            return True
        except:
            return False

    def query(self, model_name, **query_kwargs):
        """ Perform queries on data contained in the DB and return a queryset. """

        # Remove some special args that might be in query_kwargs
        qs_processor = query_kwargs.pop('qs_processor', None)
        order_by = query_kwargs.pop('order_by', None)
        try:
            objects = self.get(model_name)['objects']
        #Occurs when there's no data that's been retrieved from the server.
        except KeyError:
            objects = []

        if query_kwargs:
            objects = self.__resolve_query(objects, **query_kwargs)

        if order_by:
            if order_by[0] == '-':
                order_by_keyword = order_by[1:]
                reverse = True
            else:
                order_by_keyword = order_by
                reverse = False
                
            objects = natsorted(objects, key=lambda k: k[order_by_keyword], reverse = reverse)
        
        # Apply any processors to further manipulate the data in the qs        
        if qs_processor:
            objects = list(filter(partial(qs_processor, model_name),objects))

        qs = Queryset(self, model_name, objects, **query_kwargs)
        return qs

    def refresh_related_objects(self, obj, *args):

        related_uris = self.get_related_objects(obj)
        
        # Retrieve data for the end points
        endpoint_list = [(uri[0], join_api_path(self.remote_url, uri[1], append_slash = True)) for uri in related_uris]
        remote_data = asyncio.run(fetch_data_batch(endpoint_list,
            include_model_name = True))

        for model_name, data in remote_data:
            if self._data_is_valid(model_name, data):
                self.update_object(model_name, data)

        self.store_sync(force = True)

    def schema_configuration(self, schema = {}):
        """ Initializes the store correctly after user logs in. """
        self.model_registry.set_schema(schema = schema)

    def store_load(self):
        try:
            with open(self.filename, 'r') as fd:                
                self._data = json.load(fd)
            return True
        except FileNotFoundError:
            pass
        except:
            write_to_log("Kivy Tastypie: Failed to load DB from file.",
                level = 'error')
            return False

    def update_object(self, model_name, data, commit = False):
        db_record = self.get(model_name)
        for index, obj_json in enumerate(db_record['objects']):
            if obj_json['resource_uri'] == data['resource_uri']:
                db_record['objects'][index] = data
                break
        self.put(model_name, db_record)
        self.updated_model = data

    @property
    def _last_synch_time(self):
        """ Returns the last time data was synched with remote server. """

        last_known_time = self.get('last_synch_time')
        if last_known_time:
            return datetime.datetime.strptime(last_known_time, '%d/%m/%y')

    def __fetch_api_schema(self, *args, **kwargs):
        """ Fetch latest schema information from remote server. """

        #Retrieve and store the latest api schema's available
        api_info = asyncio.run(async_get(self.api_base_url))
        schema_list = []
        for endpoint_name, endpoint_info in api_info.items():
            schema_list.append((endpoint_name,
                join_api_path(self.remote_url, endpoint_info['schema'])))
        all_model_schema = asyncio.run(fetch_all_schema(schema_list))

        #Determine models to keep synched and correctly format tha schema
        processed_schema = {'api_info':api_info}
        for model_schema in all_model_schema:
            #Convert the list of all schema information into a dictionary
            processed_schema[model_schema['model_name']] = model_schema

        #Store this information in the local DB
        self.schema_configuration(schema = processed_schema)

    def __resolve_query(self, objects, **query_kwargs):
        """ Resolve queries based on key word arguments provided. """
        
        filtered_objects = []
        for obj in objects:
            bad_match = False 

            # Go through each search term provided. If any term is not found,
            # consider this object a bad match
            for query_term, query_value in query_kwargs.items():

                if '__in' in query_term:
                    query_term = query_term.replace('__in', '')
                    if obj[query_term].lower() not in query_value:
                        bad_match = True

                else:
                    if isinstance(query_value, str):
                        try:
                            if not query_value.lower() in obj[query_term].lower():
                                bad_match = True
                        except:
                            bad_match = True
                    else:
                        try:
                            if obj[query_term] != query_value:
                                bad_match = True
                        except:
                            bad_match = True
                        
            # Only select this object is every term in the search query returned
            # a positive match result
            if not bad_match:
                filtered_objects.append(obj)

        return filtered_objects

    def _data_is_valid(self, model_name, data):
        """ Verifies that the data passed is a valid JSON object for the model in question. """
        full_schema = self.get('schema')
        model_schema = full_schema[model_name]
        try:
            for field_name, field_schema in model_schema['fields'].items():
                if field_name not in data:
                    return False
            return True
        except:
            return False

    def _model_name_from_uri(self, uri):
        full_schema = self.get('schema')
        for model_name, model_schema in full_schema.items():
            regex_pattern = re.compile(f"/{model_name}/")
            if re.search(regex_pattern, uri):
                return model_name

    def _pre_create(self, model_name, data, *args, **kwargs):
        """ Preprocessing of the data before POST to remote. """

        #Add some extra information
        data['created'] = datetime.datetime.now()     
        data['user'] = self.data_user['resource_uri']  
        self._sanitize_obj_fields(data)
        return data
            
    def _sanitize_obj_fields(self, data):
        """ Convert all fields into the correct format required by TastyPie """

        for k,v in data.items():
            if isinstance(v, datetime.datetime):
                data[k] = v.strftime(REMOTE_DATETIME_FORMAT)
            elif isinstance(v, datetime.date):
                data[k] = v.strftime(REMOTE_DATE_FORMAT)

    def _set_last_synch_time(self):
        """ Sets last time data was synched with remote server. """

        self.put('last_synch_time',
            datetime.datetime.now().strftime('%d/%m/%y'))