from calendar import TextCalendar
from functools import partial
import datetime 
import traceback

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import (ObjectProperty, OptionProperty, NumericProperty, ListProperty, StringProperty)
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup

from kivy_tastypie.config import orm_settings

Builder.load_string('''
#: import PRIMARY_BUTTON_CLASS kivy_tastypie.config.orm_settings

<DateWidget>:

    PRIMARY_BUTTON_CLASS:
        id: day_selector
        text: root.current_day
        size_hint: (0.7, 0.15)
        pos_hint: {'top':0.9, 'center_x':0.5}

    PRIMARY_BUTTON_CLASS:
        id: month_selector
        text: root.current_month
        size_hint: (0.7, 0.15)
        pos_hint: {'top':0.7, 'center_x':0.5}

    PRIMARY_BUTTON_CLASS:
        id: year_selector
        text: root.current_year
        size_hint: (0.7, 0.15)
        pos_hint: {'top':0.5, 'center_x':0.5}

''')

class DateWidget(FloatLayout):

    calendar = TextCalendar()
    current_day = StringProperty('-')
    current_month = StringProperty('-')
    current_year = StringProperty('-')
    display_date = ObjectProperty(datetime.date.today())
    display_day = StringProperty('-')
    display_month = StringProperty('-')
    display_year = StringProperty('-')
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    popup = None

    def __init__(self, *args, **kwargs):
        self.dropdown_register = {}
        current_date = kwargs.pop('current_date', None)
        if not current_date:
            current_date = datetime.date.today()
        elif isinstance(current_date, str):
            for DateFormat in orm_settings.REMOTE_DATE_FORMAT:
                try:
                    current_date = datetime.datetime.strptime(current_date, DateFormat)
                    break
                except:
                    pass
        self.on_close = kwargs.pop('on_close', None)
        super().__init__(*args, **kwargs)
        self.display_date = current_date
        self.on_display_date()
        Clock.schedule_once(self.bind_day_dropdown, 0.05)
        Clock.schedule_once(self.bind_month_dropdown, 0.1)
        Clock.schedule_once(self.bind_year_dropdown, 0.15)
        
    def bind_day_dropdown(self, *args, **kwargs):
        year = kwargs.pop('year', self.display_date.year )
        month = kwargs.pop('month', self.display_date.month )
        month_days = kwargs.pop('month_days', list(self.calendar.itermonthdays(year,month)))
        day_list = []
        for d in month_days:
            if d != 0:
                day_list.append(d)
        self.bind_dropdown('day', self.ids['day_selector'], day_list)

    def bind_dropdown(self, dropdown_type, trigger_button, option_list, *args, **kwargs):
        """ Construct the Dropdown menu"""

        dropdown = self.get_dropdown(dropdown_type)
        #Create all possible selections

        for opt in option_list:
            btn = orm_settings.DROPDOWN_BUTTON_CLASS(text=str(opt), size_hint_y=None, height='60dp')
            btn.bind(on_release=lambda btn: dropdown.select(btn.text))
            btn.trigger = trigger_button
            dropdown.add_widget(btn)

        trigger_button.bind(on_release=dropdown.open)
        dropdown.bind(on_select=partial(self.dropdown_button_selected, trigger_button, dropdown_type))
        self.register_dropdown(dropdown_type, dropdown)

    def bind_month_dropdown(self, *args, **kwargs):
        self.bind_dropdown('month', self.ids['month_selector'], self.months)

    def bind_year_dropdown(self, *args, **kwargs):
        this_year = datetime.date.today().year
        year_list = []
        for i in range(2):
            year_list.append(this_year-i)
        year_list.reverse()
        self.bind_dropdown('year', self.ids['year_selector'], year_list)

    def confirm_max_day(self, year, month, day):
        """Confirms whether or not the given day exceeds the end of the month, e.g. 30 Feb.
        Returns the last possible date if this is the case."""

        month_days = list(self.calendar.itermonthdays(year, month))
        month_days.reverse()
        last_day = None
        for d in month_days:
            if d != 0:
                last_day = d
                break

        if day > last_day:
            return last_day
        else:
            return day

    def done(self, *args, **kwargs):
        if self.on_close:
            self.on_close(self.display_date)
        self.popup.dismiss()


    def dropdown_button_selected(self, trigger_button, dropdown_type, dropdown, value):
        year = self.display_date.year
        month = self.display_date.month
        day = self.display_date.day
        if dropdown_type == 'year':
            year = int(value)
        elif dropdown_type == 'month':
            month = self.months.index(value)+1
        elif dropdown_type == 'day':
            day = int(value)

        if self.display_date.month != month:
            rebind_days = partial(self.bind_day_dropdown, year = year, month = month)
            Clock.schedule_once(rebind_days, 0.2)
            day = self.confirm_max_day(year, month, day)

        self.display_date = datetime.datetime(year, month, day)

    def get_dropdown(self, dropdown_type):
        """ Erither returns a new dropdown object, or recyles the pre-existing dropdown """

        if dropdown_type in self.dropdown_register:
            dropdown = self.dropdown_register[dropdown_type]
            dropdown.clear_widgets()
        else:
            dropdown = DropDown()
        return dropdown

    def on_display_date(self, *args):
        """ Update widget display text with latest value selected. """
        self.current_day = self.display_date.strftime("%d")
        self.current_month = self.display_date.strftime("%b")
        self.current_year = self.display_date.strftime("%Y")

    def open(self, show_done_btn = True, *args, **kwargs):
        def_kwargs = {'title':'Enter Date',
            'content':self,'size_hint':(0.8, 0.5)}
        def_kwargs.update(kwargs)
        self.content = def_kwargs['content']
        if show_done_btn:
            self.add_widget(self._generate_close_widget())
        self.popup = Popup(**def_kwargs)
        self.popup.open()
        self.content.popup = self.popup
    
    def register_dropdown(self, dropdown_type, dropdown):
        """ Register the dropdown object to use for each type. """

        if dropdown_type not in self.dropdown_register:
            self.dropdown_register[dropdown_type] = dropdown

    def _generate_close_widget(self):
        w = BoxLayout(size_hint_y = 0.2, orientation = 'horizontal',
            pos_hint = {'center_x':0.5, 'top':0.23})
        spacing_label_kwargs = {'text':'', 'size_hint_x':0.25}
        btn = orm_settings.PRIMARY_BUTTON_CLASS(size_hint_x = 0.5, text = 'Done', markup = True,
            pos_hint = {'center_y':0.5, 'center_x':0.5},
            on_release = self.done)
        w.add_widget(Label(**spacing_label_kwargs))
        w.add_widget(btn)
        w.add_widget(Label(**spacing_label_kwargs))
        return w
        