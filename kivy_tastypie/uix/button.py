from kivy_tastypie.config import orm_settings, import_class

DropDownButton = import_class(orm_settings.DROPDOWN_BUTTON_CLASS)
PrimaryButton = import_class(orm_settings.PRIMARY_BUTTON_CLASS)