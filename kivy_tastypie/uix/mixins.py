class MatchChildHeightMixin():

    def __init__(self, *args, **kwargs):
        kwargs['size_hint_y'] = None
        super().__init__(*args, **kwargs)
        total_height = 0
        for c in self.children:
            total_height += c.height
        self.height = total_height