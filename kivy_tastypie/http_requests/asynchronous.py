import aiohttp
import asyncio
import logging
import traceback

from kivy.app import App

from .synchronous import join_api_path
from kivy_tastypie.config import orm_settings

async def async_get(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.json()

async def concurrently_post_many(request_list):
    """ Gather many HTTP call made async
    Args:
        request_list: a list of tuples of the form ([endpoint], [post_data])
    Return:
        A tuple in the form of ([response_summary], [responses]):
        response_summary: A dictionary containing information on the requests
        that were make successfully and those that failed.
        responses: List of the actual response data returned

    """
    async with aiohttp.ClientSession() as session:
        tasks = []
        for request_tuple in request_list:
            request_url = join_api_path(orm_settings.REMOTE_URL, request_tuple[0]) + '/'
            tasks.append(
                post_once(
                    session,
                    request_url,
                    request_tuple[1]
                )
            )
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        response_summary = {'okay':[], 'failed':[], 'all_okay':False}
        for r in responses:
            if issubclass(r.__class__, aiohttp.ClientError):
                response_summary['failed'].append(r)
            elif 'error_message' in r or 'error' in r:
                response_summary['failed'].append(r)
            else:
                response_summary['okay'].append(r)
        if len(response_summary['okay']) >= len(tasks):
            response_summary['all_okay'] = True

        return response_summary, responses

async def fetch_all_data(endpoint_list):
    """ Gather many HTTP call made async
    Args:
        endpoint_list: a list of tuples of the form ([model_name], [api_query_string])
    Return:
        responses: A list of dict like object containing JSON response
    """
    async with aiohttp.ClientSession() as session:
        tasks = []
        for endpoint in endpoint_list:
            tasks.append(
                fetch_data(
                    session,
                    endpoint[1],
                    endpoint[0]
                )
            )
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        return responses

async def fetch_all_schema(schema_list):
    """ Gather many HTTP call made async
    Args:
        schema_list: a list of tuples of the form ([model_name], [api_schema_endpoint])
    Return:
        responses: A list of dict like object containing JSON response
    """
    async with aiohttp.ClientSession() as session:
        tasks = []
        for schema_entry in schema_list:
            tasks.append(
                fetch_schema(
                    session,
                    schema_entry[1],
                    schema_entry[0]
                )
            )
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        return responses

async def fetch_data(session, url, model_name, include_model_name = False):
    async with session.get(url) as response:
        resp_dict = await response.json()
        if 'meta' in resp_dict:
            resp_dict['meta']['model_name'] = model_name

        if not include_model_name:
            return resp_dict
        else:
            return (model_name, resp_dict)

async def fetch_data_batch(endpoint_list, include_model_name = False):
    """ Gather many HTTP call made async
    Args:
        endpoint_list: a list of tuples of the form ([model_name], [api_query_string])
    Kwargs:
        include_model_name: 
    Return:
        responses: A list of dict like object containing JSON response, or a tuple of the form
        ([model_name], [json_response])
    """
    async with aiohttp.ClientSession() as session:
        tasks = []
        for endpoint in endpoint_list:
            tasks.append(
                fetch_data(
                    session,
                    endpoint[1],
                    endpoint[0],
                    include_model_name = include_model_name
                )
            )
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        return responses

async def fetch_schema(session, url, model_name):
    async with session.get(url) as response:
        resp_dict = await response.json()
        resp_dict['endpoint_url'] = url
        resp_dict['related_schema_name'] = '/'.join([f'/{orm_settings.API_ROOT}', model_name,
            'schema/'])
        resp_dict['model_name'] = model_name
        return resp_dict

async def post_once(session, url, data):
    async with session.post(url, json = data) as response:
        resp_dict = await response.json()
        return resp_dict