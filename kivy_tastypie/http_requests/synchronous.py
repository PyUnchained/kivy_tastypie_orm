import logging
import requests
import traceback

from kivy_tastypie.config import orm_settings

def join_api_path(*args, append_slash = False):
    sanitized_args = []
    for index, path_element in enumerate(args):
        try:
            if path_element[0] == '/':
                path_element = path_element[1:]
            if path_element[-1] == '/':
                path_element = path_element[:-1]
            sanitized_args.append(path_element)

        #Happens if the argument is actually ''
        except IndexError: 
            pass
        
    resp = "/".join(sanitized_args)
    if append_slash:
        resp = resp + '/'
    return resp



def post_new_model(model_name, data):
    headers = {'Content-type':'application/json'}
    url = join_api_path(REMOTE_URL, '/core_api', model_name) + "/"
    try:
        r = requests.post(url, json = data, headers = headers)
        resp_data = r.json()
        if 'error_message' in resp_data:
            success = False
        else:
            success = True
        return (success, resp_data)
    except:
        logging.error('Failed to POST new model to remote: ')
        logging.error(traceback.format_exc())
        return (False, {})

def request_okay(resp_data):
    if 'error_message' in resp_data or 'error' in resp_data:
        return False
    elif 'okay' in resp_data:
        return resp_data['okay']
    else:
        return True

def secure_delete(endpoint):
    url = join_api_path(orm_settings.REMOTE_URL, endpoint) + '/'
    return secure_request(url, json = data)

def secure_get(url, *args, **request_kwargs):
    if 'headers' not in request_kwargs:
        request_kwargs['headers'] = {'Content-type':'application/json'}

    url = join_api_path(orm_settings.REMOTE_URL, url)
    try:
        r = requests.get(url, **request_kwargs)
        resp_data = r.json()
        return (request_okay(resp_data), resp_data)
    except:
        logging.error('RMS: Error making GET request: ')
        logging.error(traceback.format_exc())
        return (False, {})

def secure_post(url, json = {}, headers = {}):
    if not headers:
        headers = {'Content-type':'application/json'}

    url = join_api_path(orm_settings.REMOTE_URL, url)
    if url[-1] != '/':
        url = url + '/'
        
    r = requests.post(url, json = json, headers = headers)
    resp_data = r.json()
    return (request_okay(resp_data), resp_data)

def secure_delete(url, *args, **request_kwargs):
    if 'headers' not in request_kwargs:
        request_kwargs['headers'] = {'Content-type':'application/json'}

    url = join_api_path(orm_settings.REMOTE_URL, url)
    if url[-1] != '/':
        url = url + '/'
    try:
        r = requests.delete(url, **request_kwargs)
        return (True, None)
    except:
        logging.error('RMS: Error making DELETE request: ')
        logging.error(traceback.format_exc())
        return (False, {})


def secure_request(url, **request_kwargs):
    if 'headers' not in request_kwargs:
        request_kwargs['headers'] = {'Content-type':'application/json'}
    try:
        r = requests.post(url, **request_kwargs)
        resp_data = r.json()
        return (request_okay(resp_data), resp_data)
    except:
        logging.error('Failed to POST new model to remote: ')
        logging.error(traceback.format_exc())
        return (False, {})
