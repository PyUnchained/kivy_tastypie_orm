def get_request_string(get_arg_tuples):
    """ Takes any number of kwargs, representing the name and value of the
    get request arguments. """
    
    get_str = ""
    for index, arg_tuple in enumerate(get_arg_tuples):
        if index == 0:
            arg_prefix = "?"
        else:
            arg_prefix = '&'

        get_str += f"{arg_prefix}{arg_tuple[0]}={arg_tuple[1]}"
    return get_str