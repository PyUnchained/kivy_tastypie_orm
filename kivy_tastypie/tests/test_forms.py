import unittest
from kivy.tests.common import GraphicUnitTest

from kivy_tastypie.config import orm_settings
from kivy_tastypie.forms.utils import build_form_registry, get_model_form_class
from kivy_tastypie.tests.contrib.forms import AAddForm, AEditForm, AViewForm

from kivy_tastypie.forms import Form
from kivy_tastypie.forms.fields import FormField

class FormUtilsTestCase(GraphicUnitTest):

    def test_one(self):
        pass
        try:
            import kivy_tastypie
        except:
            pass

class FormUtilsTestCase(unittest.TestCase):


    def test_build_registry(self):

        #Building registry with no form definitions
        a = build_form_registry()
        self.assertTrue('base_form_class' in a)
        self.assertEqual(1, len(a))
        
        #Building registry with user defined form definitions
        orm_settings.update_setting('FORM_DEFINITIONS', 'kivy_tastypie.tests.contrib.forms')
        a = build_form_registry()
        self.assertTrue('base_form_class' in a)
        self.assertEqual(4, len(a))

    def test_retrieving_model_form(self):
        orm_settings.update_setting('FORM_DEFINITIONS', 'kivy_tastypie.tests.contrib.forms')
        a = build_form_registry()

        self.assertEqual(AAddForm , get_model_form_class('add', 'a'))
        self.assertEqual(AEditForm, get_model_form_class('edit', 'a'))
        self.assertEqual(AViewForm, get_model_form_class('view', 'a'))

class FormTestCase(GraphicUnitTest):

    def test_building_form_class(self):

        class LoginForm(Form):
            username = FormField()
            password = FormField()

        form = LoginForm()


