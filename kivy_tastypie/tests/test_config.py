import unittest
import os

from kivy_tastypie.errors import ConfigError


class ConfigurationTestCase(unittest.TestCase):
        

    def test_check_configuration(self):
        from kivy_tastypie.config.utils import ConfigObject
        
        # Error should be raised because no settings file is given
        try:
            ConfigObject(user_defined_settings = None)
            self.assertTrue(False)
        except ConfigError:
            self.assertTrue(True)

        # Error should be raised because no path given to import Model class definitions in settings
        try:
            ConfigObject(user_defined_settings = "kivy_tastypie.tests.contrib.bad_settings")
            self.assertTrue(False)
        except ConfigError:
            self.assertTrue(True)

        #Should work fine now
        ConfigObject(user_defined_settings = "kivy_tastypie.tests.contrib.minimum_settings")
