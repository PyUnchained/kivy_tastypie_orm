import argparse
import sys, os

def start():
    username = os.environ['USERNAME']
    home_dir = os.path.join('home', username)
    os.makedirs(home_dir)
    os.system(f'useradd -p $(openssl passwd -1 password) {username}')

if __name__ == '__main__':
    start()