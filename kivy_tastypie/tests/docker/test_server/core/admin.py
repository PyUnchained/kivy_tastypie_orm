from django.contrib import admin

from core.models import BaseModel, ForeignKeyModel, ManyToManyModel, GenericForeignKeyModel

# Register your models here.
admin.site.register(BaseModel)
admin.site.register(ForeignKeyModel)
admin.site.register(ManyToManyModel)
admin.site.register(GenericForeignKeyModel)