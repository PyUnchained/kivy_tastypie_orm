from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation

# Create your models here.
class BaseTestModel(models.Model):
    char = models.CharField(blank = True, null = True, max_length = 500)
    int = models.IntegerField(blank = True, null = True)
    float = models.FloatField(blank = True, null = True)
    binary = models.BinaryField(blank = True, null = True)
    date = models.DateField(blank = True, null = True)
    date_time = models.DateTimeField(blank = True, null = True)
    decimal = models.DecimalField(blank = True, null = True,
        decimal_places = 2, max_digits = 20)
    email = models.EmailField(blank = True, null = True)
    file = models.FileField(blank = True, null = True, upload_to = 'test_docs')
    slug = models.SlugField(blank = True, null = True)
    time = models.TimeField(blank = True, null = True)

    class Meta:
        abstract = True

class BaseModel(BaseTestModel):
    gfk_relation = GenericRelation('GenericForeignKeyModel',
        content_type_field='ct',
        object_id_field='gfk_id')

class ForeignKeyModel(BaseTestModel):
    fk = models.ForeignKey('BaseModel', models.CASCADE)
    gfk_relation = GenericRelation('GenericForeignKeyModel',
        content_type_field='ct',
        object_id_field='gfk_id')

class ManyToManyModel(BaseTestModel):
    m2m = models.ManyToManyField('BaseModel')
    gfk_relation = GenericRelation('GenericForeignKeyModel',
        content_type_field='ct',
        object_id_field='gfk_id')

class GenericForeignKeyModel(BaseTestModel):
    ct = models.ForeignKey(ContentType, on_delete=models.CASCADE, null = True)
    gfk_id = models.PositiveIntegerField(null = True)
    gfk = GenericForeignKey('ct', 'gfk_id')
