from tastypie.resources import ModelResource
from core.models import BaseModel, ForeignKeyModel, ManyToManyModel, GenericForeignKeyModel

# class BaseModel(BaseTestModel):
#     gfk_relation = GenericRelation('GenericForeignKeyModel',
#         content_type_field='ct',
#         object_id_field='gfk_id')

# class ForeignKeyModel(BaseTestModel):
#     fk = models.ForeignKey('BaseModel', models.CASCADE)
#     gfk_relation = GenericRelation('GenericForeignKeyModel',
#         content_type_field='ct',
#         object_id_field='gfk_id')

# class ManyToManyModel(BaseTestModel):
#     m2m = models.ManyToManyField('BaseModel')
#     gfk_relation = GenericRelation('GenericForeignKeyModel',
#         content_type_field='ct',
#         object_id_field='gfk_id')

# class GenericForeignKeyModel(BaseTestModel):

class BaseModelResource(ModelResource):
    class Meta:
        queryset = BaseModel.objects.all()
        resource_name = 'base'

class ForeignKeyModelResource(ModelResource):
    class Meta:
        queryset = ForeignKeyModel.objects.all()
        resource_name = 'foreign_key'

class ManyToManyModelResource(ModelResource):
    class Meta:
        queryset = ManyToManyModel.objects.all()
        resource_name = 'many_to_many'

class GenericForeignKeyModelResource(ModelResource):
    class Meta:
        queryset = GenericForeignKeyModel.objects.all()
        resource_name = 'generic_foreign_key'