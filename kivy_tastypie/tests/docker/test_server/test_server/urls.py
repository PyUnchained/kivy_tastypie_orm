"""test_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import inspect

from django.contrib import admin
from django.urls import path, include

from tastypie.api import Api
from tastypie.resources import ModelResource

from core import api as tastypie_resources

#Register all API class url functions
api_urls = Api(api_name='api')

#Automatically register any objects descended from the ModelResource class
api_classes = []
for name, obj in inspect.getmembers(tastypie_resources):
    if inspect.isclass(obj):
        if issubclass(obj, ModelResource) and obj != ModelResource:
            api_classes.append(obj)

for CL in api_classes:
    api_urls.register(CL())

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(api_urls.urls)),
]
