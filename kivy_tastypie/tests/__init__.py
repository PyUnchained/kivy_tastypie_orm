import os
os.environ["ORM_SETTINGS"] = "kivy_tastypie.tests.contrib.settings"

from kivy.config import Config

#Reset some default values
Config.set('kivy', 'default_font', ["Roboto", "data/fonts/Roboto-Regular.ttf",
    "data/fonts/Roboto-Italic.ttf", "data/fonts/Roboto-Bold.ttf", "data/fonts/Roboto-BoldItalic.ttf"])
Config.set('kivy', 'log_level', 'info')
Config.write()
