import unittest

from kivy_tastypie.http_requests.utils import get_request_string

class HTTPTestCase(unittest.TestCase):
        
    def test_creating_get_request_string(self):
        resp = get_request_string([('user__username', 'cypo'), ('height', 45)])
        self.assertEqual(resp, "?user__username=cypo&height=45")