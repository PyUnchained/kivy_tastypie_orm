from kivy_tastypie.forms.base import GenericModelForm

class AAddForm(GenericModelForm):
    model_name = 'a'
    type = 'add'

class AEditForm(GenericModelForm):
    model_name = 'a'
    type = 'edit'

class AViewForm(GenericModelForm):
    model_name = 'a'
    type = 'view'