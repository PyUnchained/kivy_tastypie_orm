import unittest
import sys, os

from kivy_tastypie.config import orm_settings

class StoreTestCase(unittest.TestCase):
    def setUp(self):
        from kivy_tastypie.store.core import TastyPieMemStore
        self.test_store = TastyPieMemStore('test.json', 'api')
        
    def test_post_login_initialization(self):
        self.assertTrue(self.test_store.initial_sync_to_remote({'username':'test'}))
