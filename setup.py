#!/usr/bin/env python3.8
import os
from setuptools import setup, find_packages

def read_requirements():
    requirements_list = []
    with open('./requirements.txt') as f:
        for l in f.readlines():
            requirements_list.append(l)
    return requirements_list

setup(name='kivy_tastypie_orm',
      version='0.1.2',
      description='Interface to interact with TastyPie based ReST API through Kivy',
      author='Tatenda Tambo',
      author_email='tatendatambo@gmail.com',
      packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
      install_requires = read_requirements()
    )